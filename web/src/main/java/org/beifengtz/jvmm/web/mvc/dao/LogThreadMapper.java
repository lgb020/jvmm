package org.beifengtz.jvmm.web.mvc.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.beifengtz.jvmm.web.entity.po.LogThreadPO;

/**
 * Description: TODO
 *
 * Created in 10:33 2022/2/28
 *
 * @author beifengtz
 */
@Mapper
public interface LogThreadMapper extends BaseMapper<LogThreadPO> {
}
