package org.beifengtz.jvmm.web.mvc.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.beifengtz.jvmm.web.entity.po.NodeConfPO;

/**
 * Description: TODO
 *
 * Created in 11:21 2022/2/25
 *
 * @author beifengtz
 */
@Mapper
public interface NodeConfMapper extends BaseMapper<NodeConfPO> {
}
